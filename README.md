# Building

```
$ mkdir -p out
$ make Rot_chisel # Rot_test, Rot_icestorm, Rot_install 
```

# Dependencies

- Chisel, Yosys, NextPNR, IceStorm
- iCE40HX8K Breakout Board


# TODO

- Build system setup (chisel integration, simulation, testing)
- Tools to study
  - verilog, blif, technology mapping (yosys)
    - yosys/techlibs/ice40/ (or /usr/share/yosys/ice40/)
  - place and route (arachne-pnr)
    - icestorm/icebox/icebox_chipdb.py (or /usr/share/icebox/chipdb-8k.txt)
  - usb interface (iceprog)
  - configuration protocol
- ROM/RAM setup
  - chisel side
  - icestorm side
- FPGA/LUT implementation
- Gate level simulation (SPICE)
    - cf. yosys/examples/cmos
- Solid state, Semiconductor physics therory
- Top as BlackBox
- Debugging methods
  - prepare some ways to test pin IO signals (tristate, input, output, etc..)
  - just voltage tester?
  - debug message via Uart?
    

# References

- IceStorm
  - http://www.clifford.at/icestorm/
  - http://hedmen.org/icestorm-doc/icestorm.html
  - https://github.com/nesl/ice40_examples
- FPGA
  - http://www.latticesemi.com/Products/FPGAandCPLD/iCE40
  - iCE40LPHXFamilyDataSheet.pdf
  - SBTICETechnologyLibrary201608.pdf
- Board
  - http://www.latticesemi.com/en/Products/DevelopmentBoardsAndKits/iCE40HX8KBreakoutBoard.aspx
  - EB85.pdf
- FTDI USB Interface
  - https://www.ftdichip.com/Products/ICs/FT2232H.htm
- Interface Protocol
  - https://reference.digilentinc.com/learn/fundamentals/communication-protocols/start
  - https://www.cyberciti.biz/faq/find-out-linux-serial-ports-with-setserial/
  - linux/Documentation/usb/usb-serial.txt, drivers/usb/serial
  - https://en.wikipedia.org/wiki/I%C2%B2C#Design
- Peripherals
  - http://longrunerpro.com/a/Products/20180507/95.html
    - address: 0x27 or 0x3F
    - HD44780 chip: https://en.wikipedia.org/wiki/Hitachi_HD44780_LCD_controller
    - I2C interface chip (aka  8-bit I/O expander): http://wiki.sunfounder.cc/index.php?title=I2C_LCD2004
    - some idea from Arduino implementation
        - https://github.com/johnrickman/LiquidCrystal_I2C
        - https://github.com/duinoWitchery/hd44780/tree/master/hd44780ioClass
  - https://www.parallax.com/product/27899
    - https://github.com/nesl/ice40_examples


# Testing UART communication 

```
# you can end communication by `sudo kill socat` from other terminal
$ sudo socat -d -d stdin,raw,echo=0 /dev/ttyUSB1,raw,echo=0  
```