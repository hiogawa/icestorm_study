out_dir = ./out
src_dir = ./src
sbt = sbt -client
# arachne_pnr_opts = -d 8k -P ct256
nextpnr_opts = --hx8k --package ct256
modules = Rot RotInner Adder Subtracter Uart MatrixKeyPad MatrixKeyPadInner LCD UtilsAsymmetricQueue
src_files = $(shell find $(src_dir)/main/scala -type f)
TEST_SEED ?= $(shell date +%s%N)
TEST_BACKEND ?= treadle

%.v: $(src_files)
	$(sbt) "runMain icestorm_study.Driver --output-file $@  --target-dir $(@D) $(basename $(@F))"

%.vcd: $(src_files)
	$(sbt) 'runMain icestorm_study.TestDriver --backend-name $(TEST_BACKEND) --test-seed $(TEST_SEED) --generate-vcd-output on --target-dir $(@D) $(basename $(@F))'

%.pcf: $(src_dir)/main/resources/pcf.txt
	cp $(src_dir)/main/resources/pcf.txt $(@)

# arachne-pnr
#%.blif: %.v
#	yosys -Q -p "read_verilog $<; synth_ice40 -blif $@" | tee $@.log

#%.asc: %.pcf %.blif
#	arachne-pnr $(arachne_pnr_opts) -o $@ -p $(word 1, $^) $(word 2, $^) 2>&1 | tee $@.log

# nextpnr
%.json: %.v
	yosys -Q -p "read_verilog $<; synth_ice40; write_json $@" | tee $@.log

%.asc: %.pcf %.json
	nextpnr-ice40 $(nextpnr_opts) --pcf $(word 1, $^) --json $(word 2, $^) --asc $@ 2>&1 | tee $@.log

%.ex: %.asc
	icebox_explain $< > $@

%.bin: %.asc
	icepack $< $@


# This generates below:
# Rot_chisel: ./out/Rot/Rot.v
# etc ...
define MODULE_template =
.PHONY: $(1)_clean $(1)_sim
$(1)_chisel: $(out_dir)/$(1)/$(1).v
$(1)_sim: $(out_dir)/$(1)/$(1).vcd
$(1)_icestorm: $(out_dir)/$(1)/$(1).asc
$(1)_install: $(out_dir)/$(1)/$(1).bin
	iceprog $$<
$(1)_clean:
	rm -rf $(out_dir)/$(1)
endef
$(foreach module,$(modules),$(eval $(call MODULE_template,$(module))))

clean:
	rm -rf $(addprefix $(out_dir)/,$(modules))

.PHONY: clean default
.DEFAULT_GOAL := default
default: ;
.PRECIOUS: %.v %.vcd %.pcf %.blif %.asc %.ex %.json %.bin
