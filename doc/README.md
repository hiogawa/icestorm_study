Links to the documents

- Tools

blif.pdf [Berkeley Logic Interchange Format ( BLIF )](https://www.cse.iitb.ac.in/~supratik/courses/cs226/spr16/blif.pdf)
firrtl_spec.pdf [Flexible Intermediate Representation for RTL](https://github.com/freechipsproject/firrtl/blob/master/spec/spec.pdf)
ngspice_manual.pdf [ngspice user manual](http://ngspice.sourceforge.net/docs/ngspice-manual.pdf)
yosys_manual.pdf [Yosys Manual](http://www.clifford.at/yosys/files/yosys_manual.pdf)


- FTDI

DS_FT2232H.pdf [FT2232H Dual High Speed USB to Multipurpose UART/FIFO IC](https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT2232H.pdf)
AN_135_MPSSE_Basics.pdf [FTDI MPSSE Basics](https://www.ftdichip.com/Support/Documents/AppNotes/AN_135_MPSSE_Basics.pdf)


- Lattice

EB85.pdf [iCE40HX-8K Breakout Board User’s Guide](?)
FPGA-TN-02001-3-1-iCE40-Programming-Configuration.pdf [iCE40 Programming and Configuration](?)
FPGA-TN-02024-4-1-PCB-Layout-Rec-BGA-Packages.pdf [PCB Layout Recommendations for BGA Packages](?)
iCE40HardwareChecklist.pdf [TN1252 - iCE40 Hardware Checklist](?)
iCE40LPHXFamilyDataSheet.pdf [DS1040 - iCE40 LP/HX Family Data Sheet](?)
MemoryUsageGuideforiCE40Devices.pdf [TN1250 - Memory Usage Guide for iCE40 Devices](?)
SBTICETechnologyLibrary201608.pdf [SiliconBlue ICE Technology Library](?)


- Periferals

PCF8574T_datasheet.pdf [PCF8574 Remote 8-bit I/O expander for I2C-bus](http://wiki.sunfounder.cc/images/1/18/PCF8574T_datasheet.pdf)
HD44780.pdf [HD44780U (LCD-II) (Dot Matrix Liquid Crystal Display Controller/Driver)](https://www.sparkfun.com/datasheets/LCD/HD44780.pdf)


- Few scripts to help make this list

```
function get_pdf_title() {
  pdftk "${1}" dump_data_utf8 output | ruby -e 'puts STDIN.read[/InfoKey: Title\nInfoValue: (.*)/, 1]'
}
for f in *.pdf; do echo -n "${f} "; get_pdf_title $f; done

function wget_with_link_entry() {
  local url="${1}"
  local filename="${url##*\/}"
  wget "${url}"
  local title=$(get_pdf_title "${filename}")
  echo "(${filename}) [${title}](${url})"
}
wget_with_link_entry https://www.cse.iitb.ac.in/~supratik/courses/cs226/spr16/blif.pdf
```
