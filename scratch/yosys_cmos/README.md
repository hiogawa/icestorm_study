Original code is from [Yosys (/examples/cmos)](https://github.com/YosysHQ/yosys).
Here I removed the part using [abc](https://people.eecs.berkeley.edu/~alanmi/abc/)
so that it's much easier to follow what Yosys is doing.
In Yosys.ipynb, I broke down each synthesis step and
it shows all the relavant intermediate results of passes used in counter.ys.  

```
# Synthesize counter.v into Verilog netlist (out/counter_synth.v) and SPICE netlist (out/counter_synth.sp) 
$ yosys -Q counter.ys | tee out/yosys.log

# Simulate Verilog netlist
$ iverilog -o out/counter_tb.vvp out/counter_synth.v counter_tb.v cmos_cells.v
$ vvp out/counter_tb.vvp
$ gtkwave out/counter_tb.vcd

# Simulate SPICE netlist
$ ngspice -b testbench.sp
```

Actually there seems a bug in the 3rd bit of counter (the 3rd bit falls when enable-bit falling).
Where this comes from is a big question.
Considering iverilog simulator behaves alright, it must be ngspice related parts (yosys backend or testbench code). 


References

- [yosys_kernel](https://gitlab.com/hiogawa/yosys_kernel) 