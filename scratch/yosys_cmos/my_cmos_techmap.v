// It should cover all simcells.v, but here we only need NOT, OR, AND, XOR, MUX
// (cf. common/gate2lut.v)

// It seems there's two equivalent ways of achieving this
// - module \$_AND_ ... _TECHMAP_REPLACE_ ...
//   or
// - (* techmap_celltype = "$_AND_" *)

module \$_NOT_ (input A, output Y);
    NOT _TECHMAP_REPLACE_ (
        .A (A), .Y (Y)
    );
endmodule

module \$_OR_ (input A, input B, output Y);
    wire C;

    NOT _TECHMAP_REPLACE_ (
        .A (C), .Y (Y)
    );

    NOT or_map1 (
        .A (A), .B (B), .Y (Y)
    );
endmodule

module \$_AND_ (A, B, Y);
    input A, B;
    output Y;

    wire C;

    NOT _TECHMAP_REPLACE_ (
        .A (C), .Y (Y),
    );

    NAND and_map2 (
        .A (A), .B (B), .Y (C),
    );
endmodule

// a XOR b <==> (a | b) & (!a | !b)
//         <==> !!(a | b)) & !(a & b)
//         <==> !(!(a | b) | !!(a & b))
//         <==> (a NOR b) NOR !(a NAND b)
(* techmap_celltype = "$_XOR_" *)
module my_cmos_xor (A, B, Y);
    input A, B;
    output Y;

    wire C;
    wire D;
    wire E;
    wire F;

    NAND xor_map1 (
        .A (A), .B (B), .Y (C),
    );

    NOT xor_map2 (
        .A (C), .Y (D),
    );

    NOR xor_map3 (
        .A (A), .B (B), .Y (E),
    );

    NOR xor_map4 (
        .A (D), .B (E), .Y (Y),
    );
endmodule

// MUX a b s
// truth conditions are
// a b s
// 1 0 0
// 1 1 0
// 0 1 1
// 1 1 1
// it's not hard to see that it's equivalent to
// (a & !s) | (b & s) <==> !!(a & !s) | !!(b & s)
//                    <==> !(!(a & !s) & !(b & s))
//                    <==> (a NAND !s) NAND (b NAND s)
(* techmap_celltype = "$_MUX_" *)
module my_cmos_mux (A, B, S, Y);
    input A, B, S;
    output Y;

    wire C; // !s
    wire D; // a NAND !s
    wire E; // b NAND s

    NOT mux_map1 (
        .A (S), .Y (C),
    );

    NAND mux_map2 (
        .A (A), .B (C), .Y (D),
    );

    NAND mux_map3 (
        .A (B), .B (S), .Y (E),
    );

    NAND mux_map4 (
        .A (D), .B (E), .Y (Y),
    );
endmodule
