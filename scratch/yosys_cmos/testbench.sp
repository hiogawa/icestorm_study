* supply voltages (NOTE: here "0" doesn't mean numerical value 0, it's just a name of the node "0")
.global Vss Vdd
V1 Vss 0 DC 0
V2 Vdd 0 DC 3

* simple transistor model
.MODEL cmosn NMOS LEVEL=1 VT0=0.7 KP=110U GAMMA=0.4 LAMBDA=0.04 PHI=0.7
.MODEL cmosp PMOS LEVEL=1 VT0=-0.7 KP=50U GAMMA=0.57 LAMBDA=0.05 PHI=0.8

* load design and library
.include cmos_cells.sp
.include out/counter_synth.sp

* input signals
V3 clk 0 PULSE(0 3   1 0.1 0.1 0.8 2)
V4 rst 0 PULSE(0 3 0.5 0.1 0.1 2.9 40)
V5 en  0 PULSE(0 3 0.5 0.1 0.1 5.9 8)
* V4 rst 0 PULSE(0 3 0.5 0.1 0.1 2.9 40)
* V5 en  0 PULSE(0 3 0.5 0.1 0.1 5.5 8)

X1 clk rst en out0 out1 out2 COUNTER

* simulation script
.control
save clk rst en out0 out1 out2
tran 0.01s 50s
set wr_singlescale
set wr_vecnames
wrData out/testbench.sp.out clk rst en out0 out1 out2
shell python ../spice_plot.py --step 4 out/testbench.sp.out
.endc

.end
