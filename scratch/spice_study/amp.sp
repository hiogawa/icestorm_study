* cf. ngspice manual (chapter 21)

V1 vcc gnd DC 12
V2 vin gnd DC 0 AC 1 sin(0 1 1k)
C1 vin base 10uF
R1 vcc base 100k
R2 vcc coll 3.9k
R3 base gnd 24k
R4 emit gnd 1k
Q1 coll base emit QMOD

.model QMOD npn

.control
tran 1e-5 2e-3
set wr_singlescale
set wr_vecnames
wrData out/amp.sp.out vin vcc base emit coll
shell python ../spice_plot.py out/amp.sp.out
.endc