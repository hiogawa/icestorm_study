* Goal: implement and simulate some basic digital circuit primitives

.global Vss Vdd

* MOSFET models
.MODEL cmosn NMOS LEVEL=1 VT0=0.7 KP=110U GAMMA=0.4 LAMBDA=0.04 PHI=0.7
.MODEL cmosp PMOS LEVEL=1 VT0=-0.7 KP=50U GAMMA=0.57 LAMBDA=0.05 PHI=0.8

*** Logic circuit primitives ***
.SUBCKT NOT A Y
M1 Y A Vdd Vdd cmosp L=1u W=10u
M2 Y A Vss Vss cmosn L=1u W=10u
.ENDS NOT


*** Main circuit definition ***
V1 Vss gnd DC 0
V2 Vdd gnd DC 3
V3 clk gnd DC 0 PULSE(0 3 1 0.1 0.1 0.8 2)
    * PULSE arguments (base voltage, pulse voltage, delay, rising duration, falling duration, top duration, period)

X1 clk out1 NOT


*** Simulation ***
.control
tran 0.01s 10s
set wr_singlescale
set wr_vecnames
wrData out/logic_circuit.sp.out clk out1
shell python ../spice_plot.py --step 4 out/logic_circuit.sp.out
.endc