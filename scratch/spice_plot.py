import argparse
import matplotlib.pyplot as plt
import numpy as np

# $ python spice_plot.py -h

def plot(filename, step=0, grid=True, **ax_kwargs):
    raw = open(filename).read()
    lines = [ l.split() for l in raw.strip().splitlines()]
    header = lines[0]
    data = np.array(lines[1:], dtype=np.double)
    xs = data[:,0]  # time
    ys = data[:,1:] # signals
    num_signals = ys.shape[1]
    _, ax = plt.subplots()
    for i in range(num_signals):
        ax.plot(xs, ys[:,i] + step * i, label=header[i+1])
    if step != 0:
        ax_kwargs['yticks'] = np.arange(num_signals) * step
        ax_kwargs['yticklabels'] = ['' for _ in range(num_signals)]
    ax.set(**ax_kwargs)
    ax.grid(grid) # TODO: grid should affect xticks
    ax.legend()
    plt.show()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', type=str)
    parser.add_argument('--step', default=0, type=float,
        help='voltage to separate signals so that they don\'t overlap (useful when plotting digital signals)')
    parser.add_argument('--grid', action='store_true', default=True, help='plot with showing grid')
    # parser.add_argument('--grid-xstart', default=0, type=float)
    # parser.add_argument('--grid-xstep', default=1, type=float)
    args = vars(parser.parse_args())
    plot(args.pop('filename'), **args)