ThisBuild / version := "1.0.0"
ThisBuild / scalaVersion := "2.12.7"
ThisBuild / organization := "net.hiogawa"

lazy val topProject = (project in file("."))
  .settings(
    name := "icestorm_study",
    libraryDependencies ++= Seq(
      "edu.berkeley.cs" %% "chisel3" % "3.1.+",
      "edu.berkeley.cs" %% "chisel-iotesters" % "1.2.+",
      "com.lihaoyi" %% "pprint" % "0.5.3",
      "org.scalatest" %% "scalatest" % "3.0.5" % Test
    ),
    scalacOptions ++= Seq(
        "-deprecation", "-feature", "-unchecked",
        // cf. https://github.com/freechipsproject/chisel-template/blob/release/build.sbt#L3
        "-language:reflectiveCalls", "-Xsource:2.11")
  )
