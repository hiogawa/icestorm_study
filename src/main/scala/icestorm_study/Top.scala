package icestorm_study

import chisel3._
import chisel3.core.Analog

class TopBundle extends Bundle {
  val led = Output(UInt(8.W))
  val rs232Tx = Output(Bool())
  val rs232Rx = Input(Bool())

  // Drive rows and listen columns
  val keypad_rows = Output(Vec(4, Bool()))
  val keypad_columns = Input(Vec(4, Bool()))

  // I2C pins
  val i2c_vcc = Output(Bool())
  val i2c_scl = Output(Bool())
  val i2c_sda = Analog(1.W)
}

abstract class TopImpl extends Module {
  val io = IO(new TopBundle)

  // Default connection (child class's constructor can overwrite them)
  io.led := 0.U
  io.rs232Tx := 0.U
  io.keypad_rows := VecInit(Seq.fill(4){true.B})
  io.i2c_vcc := 1.U
  io.i2c_scl := 1.U
}

class Top(genModule: () => TopImpl) extends Module {
  val io = IO(new TopBundle)

  Utils.withInit() { init =>
    val impl = Module(genModule())

    io <> impl.io

    // During init
    when (init) {
      io.led := 0.U
      io.rs232Tx := 0.U
    }
  }
}
