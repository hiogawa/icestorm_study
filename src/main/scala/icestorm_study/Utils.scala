package icestorm_study

import chisel3._
import chisel3.core.{withClock, withReset}
import chisel3.util._

import scala.math.sqrt

object Utils {
  class CounterESR(mod: Long) extends Module {
    val width = log2Ceil(mod)
    val io = IO(new Bundle {
      val en = Input(Bool())
      val set = Input(Bool())
      val in = Input(UInt(width.W))
      val reset = Input(Bool())
      val out = Output(UInt(width.W))
      val clk = Output(Bool())
    })
    val counter = RegInit(0.U(width.W))
    io.out := counter
    io.clk := counter === (mod - 1).U
    when (io.set) {
      counter := io.in
    } .elsewhen (io.en) {
      counter := wrap(counter + 1.U, mod.U)
    } .elsewhen (io.reset) {
      counter := 0.U
    }
  }

  class Counter(mod: Long) extends Module {
    val width = log2Ceil(mod)
    val io = IO(new Bundle {
      val out = Output(UInt(width.W))
      val clk = Output(Bool())
    })
    val ESR = Module(new CounterESR(mod))
    ESR.io.en  := true.B
    ESR.io.set := false.B
    ESR.io.in  := 0.U
    ESR.io.reset  := false.B
    io.out     := ESR.io.out
    io.clk     := ESR.io.clk
  }

  class CounterR(mod: Long) extends Module {
    val width = log2Ceil(mod)
    val io = IO(new Bundle {
      val reset = Input(Bool())
      val out = Output(UInt(width.W))
      val clk = Output(Bool())
    })
    val ESR = Module(new CounterESR(mod))
    ESR.io.en  := true.B
    ESR.io.set := false.B
    ESR.io.in  := 0.U
    ESR.io.reset  := io.reset // R
    io.out     := ESR.io.out
    io.clk     := ESR.io.clk
  }

  class CounterER(mod: Long) extends Module {
    val width = log2Ceil(mod)
    val io = IO(new Bundle {
      val en = Input(Bool())
      val reset = Input(Bool())
      val out = Output(UInt(width.W))
      val clk = Output(Bool())
    })
     val ESR = Module(new CounterESR(mod))
     ESR.io.en  := io.en       // E
     ESR.io.set := false.B
     ESR.io.in  := 0.U
     ESR.io.reset  := io.reset // R
     io.out     := ESR.io.out
     io.clk     := ESR.io.clk
  }

  class AsymmetricQueue(inWidth: Int, outWidth: Int, queueSize: Int) extends Module {
    // For now, it doesn't support inWidth < outWidth
    require(inWidth >= outWidth && inWidth % outWidth == 0)
    val inFactor = inWidth / outWidth
    val qDataWidth = outWidth
    val qDataW = outWidth.W
    val qSize = queueSize
    val qPtrW = log2Ceil(qSize + inFactor).W // + inFactor for accomodating isFull computation

    val io = IO(new Bundle {
      val inReq = DeqIO(UInt(inWidth.W))
      val outReq = EnqIO(UInt(outWidth.W))
    })

    val queue = Reg(Vec(qSize, UInt(qDataW)))
    val inPtr = RegInit(0.U(qPtrW))
    val isEmpty = inPtr === 0.U
    val isFull = inPtr + inFactor.U >= qSize.U

    io.inReq.ready := !isFull
    io.outReq.noenq()

    when (!isFull && io.inReq.valid) {
      for (i <- 0 until inFactor) {
        queue(inPtr + i.U) := io.inReq.bits((i + 1) * qDataWidth - 1, i * qDataWidth)
      }
      inPtr := inPtr + inFactor.U

    // Handle dequeue only when no enqueue
    } .elsewhen(!isEmpty && io.outReq.ready) {
      // Shift registers
      io.outReq.enq(queue(0.U))
      for (i <- 1 until qSize) {
        queue((i - 1).U) := queue(i)
      }
      inPtr := inPtr - 1.U
    }
  }

  def withClockHz[T](outHz: Long, inHz: Long = Config.hwclkHz)(block: => T): T = {
    require(inHz % outHz == 0 && inHz > outHz)
    val counter = Module(new Counter(inHz / outHz))
    val clk = RegInit(false.B)
    clk := counter.io.out === 0.U
    withClock(clk.asClock) {
      withInit(1)(_ => block)
    }
  }

  // Trigger reset for first num_clks
  def withInit[T](num_clks: Int = 1)(block: Bool => T): T = {
    val reset = RegInit(false.B)
    val count = RegInit(0.U(log2Ceil(num_clks + 1).W))
    reset := true.B
    count := count + 1.U
    when (count === num_clks.U) {
      reset := false.B
      count := num_clks.U
    }
    withReset(reset)(block(count < num_clks.U))
  }

  def wrap(n: UInt, m: UInt, incremental: Boolean = true, once: Boolean = true): UInt = {
    // It feels Yosys can handle these optimization (first two from the last one)
    if (incremental) {
      Mux(n === m, 0.U, n)
    } else if (once) {
      Mux(n >= m, n - m, n)
    } else {
      n % m
    }
  }

  // Byte to Ascii with decimal digits
  def uintToAscii(n: UInt) : Seq[UInt] = {
    // TODO: implement arbitrary width (at compile time)
    //       implement without division? binary search? chinese remainder?
    val d1 = n % 10.U
    val d2 = (n / 10.U) % 10.U
    val d3 = (n / 100.U) % 10.U
    val a1 = '0'.U + d1
    val a2 = '0'.U + d2
    val a3 = '0'.U + d3
    Seq(a3, a2, a1)
  }

  // Primes below 2 ** w
  def primes(w : Int) : Seq[Int] = {
    require(1 <= w && w <= 16) // Can't produce too big primes
    val N = BigInt(2).pow(w).toInt
    val isPrime = collection.mutable.Seq.fill(N){ true }
    val iMax = sqrt(N).ceil.toInt - 1 // iMax^2 < N <= (iMax + 1)^2
    for (i <- 2 to iMax) {
      var j = 0
      while (i * (i + j) < N) {
        isPrime(i * (i + j)) = false
        j += 1
      }
    }
    (2 until N).filter(isPrime(_))
  }

  def truthTableLookup(key: UInt, table: String) : UInt = {
    val truthTableSeq : Seq[(UInt, UInt)] = table.trim.lines.map { l =>
      val in_out = l.split(' ')
      (("b_" ++ in_out(0)).U, ("b_" ++ in_out(1)).U)
    }.toSeq
    MuxLookup(key, 0.U, truthTableSeq)
  }
}
