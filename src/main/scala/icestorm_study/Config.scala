package icestorm_study

object Config {
  var hwclkHz : Long = 12 * BigInt(10).pow(6).toLong
  var rs232BaudRate : Long = 9600
  var baudPeriod : Long = Config.hwclkHz / Config.rs232BaudRate
}