package icestorm_study

import chisel3.iotesters.PeekPokeTester

class AsymmetricQueueTester(circuit: Utils.AsymmetricQueue) extends PeekPokeTester(circuit) {
  //
  poke(circuit.io.inReq.valid, 1)
  poke(circuit.io.inReq.bits, (0x3 << 16) | (0x2 << 8) | (0x1 << 0))
  step(1)
  expect(circuit.io.outReq.valid, 0)

  poke(circuit.io.outReq.ready, 1)

  // NOTE: Enque logic is not clocked (combinational),
  //       which we cannot test since recomputation has to come with register update too
  //  expect(circuit.io.outReq.valid, 1)
  //  expect(circuit.io.outReq.bits, 0x1 << 0)

  poke(circuit.io.inReq.valid, 0)
  step(1)
  expect(circuit.io.outReq.valid, 1)
  expect(circuit.io.outReq.bits, 0x2 << 0)

  step(1)
  expect(circuit.io.outReq.valid, 1)
  expect(circuit.io.outReq.bits, 0x3 << 0)
}

object UtilsTest {
  val moduleMap : Driver.ModuleMap = Map(
    "UtilsAsymmetricQueue" -> Driver.ModuleRegistration(
      () => ???,
      () => new Utils.AsymmetricQueue(8 * 3, 8, 8),
      m => new AsymmetricQueueTester(m.asInstanceOf[Utils.AsymmetricQueue]))
  )
}