package icestorm_study.module

import chisel3._
import chisel3.core.{Analog, BlackBox, RawParam, StringParam}

// cf.
// - iCE40LPHXFamilyDataSheet.pdf (sysIO)
// - SBTICETechnologyLibrary201608.pdf (SB_IO)
// - <Yosys-repo>/techlibs/ice40/cells_sim.v (SB_IO implementation for simulation)
// - https://github.com/nesl/ice40_examples (button examples use SB_IO explicitly)

// Verilog module signiture from yosis's cells_sim.v:
// module SB_IO (
//   inout  PACKAGE_PIN,
//   input  LATCH_INPUT_VALUE,
//   input  CLOCK_ENABLE,
//   input  INPUT_CLK,
//   input  OUTPUT_CLK,
//   input  OUTPUT_ENABLE,
//   input  D_OUT_0,
//   input  D_OUT_1,
//   output D_IN_0,
//   output D_IN_1
// );
//   parameter [5:0] PIN_TYPE = 6'b000000;
//   parameter [0:0] PULLUP = 1'b0;
//   parameter [0:0] NEG_TRIGGER = 1'b0;
//   parameter IO_STANDARD = "SB_LVCMOS";
//   ...
// endmodule
class SB_IO(params: Map[String, String]) extends BlackBox(params.mapValues(RawParam)) {
  val io = IO(new Bundle() {
    val PACKAGE_PIN       = Analog(1.W)
    val LATCH_INPUT_VALUE = Input(UInt(1.W))
    val CLOCK_ENABLE      = Input(UInt(1.W))
    val INPUT_CLK         = Input(UInt(1.W))
    val OUTPUT_CLK        = Input(UInt(1.W))
    val OUTPUT_ENABLE     = Input(UInt(1.W))
    val D_OUT_0           = Input(UInt(1.W))
    val D_OUT_1           = Input(UInt(1.W))
    val D_IN_0            = Output(UInt(1.W))
    val D_IN_1            = Output(UInt(1.W))
  })
}

// Wrapper for tristate IO (e.g. I2C's SDA line)
class SB_IO_tristate extends Module {
  val io = IO(new Bundle{
    val pin = Analog(1.W)
    val in = Output(UInt(1.W))
    val float = Input(Bool())
    val out = Input(UInt(1.W))
  })
  // input/output with controlled tristate mode
  val sbio = Module(new SB_IO(Map("PIN_TYPE" -> "6\\'b1010_01",
                                  "PULLUP"   -> "1\\'b1")))

  sbio.io.PACKAGE_PIN <> io.pin
  sbio.io.LATCH_INPUT_VALUE := 0.U
  sbio.io.CLOCK_ENABLE      := 0.U
  sbio.io.INPUT_CLK         := 0.U
  sbio.io.OUTPUT_CLK        := 0.U
  sbio.io.OUTPUT_ENABLE     := !io.float
  sbio.io.D_OUT_0           := io.out
  sbio.io.D_OUT_1           := 0.U
  io.in := sbio.io.D_IN_0
}
