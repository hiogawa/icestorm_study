package icestorm_study.module

import chisel3._
import chisel3.util._
import icestorm_study.{Config, Driver, TopImpl, Utils}

// cf. <icestorm-repo>/examples/icestick/rs232demo.v

// Send ASCII-encoded prime number each second via rs232
class Uart extends TopImpl {

  //// Transmittion test ////

  // Prime table
  val primes = Utils.primes(8)
  val table = VecInit(primes.map(_.U(8.W)))
  val asciiLen = 5

  // States
  val tblIndex = RegInit(0.U(log2Ceil(primes.length).W))
  val txModule = Module(new UartTx)
  val queue = Module(new Utils.AsymmetricQueue(8 * asciiLen, 8, 32))
  val asciiInReq = Wire(EnqIO(UInt((8 * asciiLen).W)))
  val secCounter = Module(new Utils.CounterR(Config.hwclkHz * 1))

  // Connections
  queue.io.inReq <> asciiInReq
  txModule.io.txReq <> queue.io.outReq
  io.rs232Tx := txModule.io.rs232Tx

  secCounter.io.reset := false.B
  asciiInReq.noenq()
  when(secCounter.io.clk && asciiInReq.ready) {
    // Just make sure width/size inferred right
    val asciis = Wire(Vec(asciiLen, UInt(8.W)))
    asciis := Utils.uintToAscii(table(tblIndex)) :+ '\r'.U :+ '\n'.U
    asciiInReq.enq(asciis.asUInt)
    tblIndex := Utils.wrap(tblIndex + 1.U, primes.length.U)
  }


  //// Reception test ////

  val ledState = RegInit(0.U(8.W))
  val rxModule = Module(new UartRx)
  val recvReq = Wire(DeqIO(UInt(8.W))) // ready bit unused

  rxModule.io.recvReq <> recvReq
  rxModule.io.rs232Rx := io.rs232Rx
  io.led := ledState.asUInt
  recvReq.nodeq() // Drive unused ready bit
  when (recvReq.valid) {
    ledState := recvReq.bits
  }
}

class UartTx extends Module {
  val io = IO(new Bundle {
    val txReq = DeqIO(UInt(8.W))
    val rs232Tx = Output(Bool())
  })

  // Utils
  val sIdle :: sStartBit :: sDataBit :: sStopBit :: Nil = Enum(4)

  // States
  val counter = Module(new Utils.CounterR(Config.baudPeriod))
  val state = RegInit(sIdle)
  val txByte = Reg(UInt(8.W))
  val txBitIndex = Reg(UInt(3.W))

  // Connections
  counter.io.reset := false.B
  io.txReq.ready := state === sIdle
  io.rs232Tx := 1.U

  switch (state) {
    is (sIdle) {
      when (io.txReq.valid) {
        txByte := io.txReq.bits
        counter.io.reset := true.B
        state := sStartBit
      }
    }
    is (sStartBit) {
      io.rs232Tx := 0.U
      when (counter.io.clk) {
        state := sDataBit
        txBitIndex := 0.U
      }
    }
    is (sDataBit) {
      io.rs232Tx := txByte(txBitIndex)
      when (counter.io.clk) {
        txBitIndex := txBitIndex + 1.U
        when (txBitIndex === 7.U) {
          state := sStopBit
        }
      }
    }
    is (sStopBit) {
      io.rs232Tx := 1.U
      when (counter.io.clk) {
        state := sIdle
      }
    }
  }
}

class UartRx extends Module {
  val io = IO(new Bundle {
    val recvReq = EnqIO(UInt(8.W))
    val rs232Rx = Input(Bool())
  })

  // Utils
  val sIdle :: sCheckStartBit :: sDataBit :: Nil = Enum(3)

  // States
  val counter = Module(new Utils.CounterR(Config.baudPeriod))
  val state = RegInit(sIdle)
  val rxByte = Reg(Vec(8, UInt(1.W)))
  val rxBitIndex = Reg(UInt(3.W)) // 0..7

  // Connections
  io.recvReq.noenq()
  counter.io.reset := false.B

  switch(state) {
    is(sIdle) {
      when(!io.rs232Rx) {
        state := sCheckStartBit
        counter.io.reset := true.B
      }
    }
    is(sCheckStartBit) {
      when(!io.rs232Rx) {
        when (counter.io.out === (Config.baudPeriod / 2).U) {
          state := sDataBit
          rxBitIndex := 0.U
        }
      } .otherwise {
        state := sIdle
      }
    }
    is(sDataBit) {
      when(counter.io.out === (Config.baudPeriod / 2).U) {
        rxByte(rxBitIndex) := io.rs232Rx
        rxBitIndex := rxBitIndex + 1.U
        when (rxBitIndex === 7.U) {
          state := sIdle
          io.recvReq.enq(rxByte.asUInt)
        }
      }
    }
  }
}

object Uart {
  val moduleMap : Driver.ModuleMap = Map(
    "Uart" -> Driver.ModuleRegistration(
      () => new Uart,
      () => ???,
      m => ???)
  )
}