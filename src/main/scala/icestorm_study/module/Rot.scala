package icestorm_study.module

import chisel3._
import chisel3.util._
import chisel3.iotesters.PeekPokeTester
import icestorm_study.{Driver, TopImpl, Utils}

class Rot extends TopImpl {
  Utils.withClockHz(16) {
    val inner = Module(new RotInner2)
    io.led := inner.io.out
  }
}

class RotInner1 extends Module {
  val io = IO(new Bundle {
    val out = Output(UInt(8.W))
  })
  val state = RegInit(1.U(8.W))
  io.out := state
  state := Cat(state(6, 0), state(7))
}

class RotInner2 extends Module {
  val io = IO(new Bundle {
    val out = Output(UInt(8.W))
  })
  val state = RegInit(1.U(8.W))
  val sLeft :: sRight :: Nil = Enum(2)
  val direction = RegInit(sLeft)
  io.out := state
  when (direction === sLeft) {
    state := state << 1.U
    when (state(7)) {
      state := state >> 1.U
      direction := sRight
    }
  } .otherwise {
    state := state >> 1.U
    when (state(0)) {
      state := state << 1.U
      direction := sLeft
    }
  }
}

class RotTester(c: RotInner1) extends PeekPokeTester(c) {
  val timerStep = 1
  expect(c.io.out, BigInt("00000001", 2))
  step(timerStep)
  expect(c.io.out, BigInt("00000010", 2))
  step(timerStep * 2)
  expect(c.io.out, BigInt("00001000", 2))
}

object Rot {
  val moduleMap : Driver.ModuleMap = Map(
    "Rot" -> Driver.ModuleRegistration(
      () => new Rot,
      () => ???,
      m => ???),
    "RotInner" -> Driver.ModuleRegistration(
      () => ???,
      () => new RotInner1,
      m => new RotTester(m.asInstanceOf[RotInner1]))
  )
}