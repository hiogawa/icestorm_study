package icestorm_study.module

import chisel3._
import chisel3.core.{Analog}
import chisel3.util._
import icestorm_study.{Config, Driver, TopImpl, Utils}

// cf.
// HD44780.pdf (Table 4 (character code (roughly ascii works)), Table 11 (example instructions))
// PCF8574T_datasheet.pdf (Chapter 6)

// Show "Hello World." on display
class LCD extends TopImpl {
  val text = VecInit("Hello World.".map(_.U))
  val textIndex = RegInit(0.U(log2Ceil(text.length).W))

  val secCounter = Module(new Utils.Counter(Config.hwclkHz))
  val inner = Module(new LCDInner)
  val writeReq = Wire(DeqIO(UInt(8.W)))
  inner.io.writeReq <> writeReq
  io.i2c_scl := inner.io.scl
  io.i2c_sda <> inner.io.sda

  writeReq.noenq()
  when (secCounter.io.clk) {
    when (writeReq.ready) {
      writeReq.enq(text(textIndex))
      textIndex := Utils.wrap(textIndex + 1.U, text.length.U)
    }
  }

  // Show slave nack sign for debugging
  when (inner.io.initFailed) {
    io.led := inner.io.initFailedIndex
  }
}

// Handle HD44780 instruction
// For starter, it only supports
// - initialize display
// - write character from left to right
//
// According to some Arduino implementation, general strategy is:
// - initialize display to 4bit communication mode
// - then after that, <rs>, <db> bits can be sent as:
//     Cat(db(7, 4), 'b_000'.U, rs) and
//     Cat(db(3, 0), 'b_000'.U, rs)
class LCDInner extends Module {
  val io = IO(new Bundle {
    val writeReq = DeqIO(UInt(8.W))
    val nack = Output(Bool())
    val initFailed = Output(Bool())
    val initFailedIndex = Output(UInt(8.W))

    // hw
    val scl = Output(Bool())
    val sda = Analog(1.W)
  })

  val sInit :: sInitFailed :: sIdle :: sSendHigh :: sSendLow :: Nil = Enum(5)
  // cf. HD44780.pdf (Table 11)
  val initDbHighs = VecInit(Seq(
    "b_0010".U, // Function set (setup 4-bit mode)
    "b_0010".U,
    "b_0000".U,
    "b_0000".U, // Display on/off control
    "b_1110".U,
    "b_0000".U, // Entry mode set
    "b_0110".U,
  ))

  val state = RegInit(sInit)
  val initIndex = RegInit(0.U(log2Ceil(initDbHighs.length).W))
  val i2cMod = Module(new I2C)
  val i2cSendReq = Wire(EnqIO(UInt(8.W)))
  val rs = RegInit(0.U(1.W))
  val db = Reg(UInt(8.W))
  i2cMod.io.sendReq <> i2cSendReq
  io.sda <> i2cMod.io.sda
  io.scl := i2cMod.io.scl
  io.nack := i2cMod.io.nack

  io.writeReq.ready := state === sIdle
  i2cSendReq.noenq()

  // Debugging
  val initFailedIndex = Reg(UInt(8.W))
  io.initFailed := state === sInitFailed
  io.initFailedIndex := initFailedIndex

  switch (state) {
    is (sInit) {
      when (i2cMod.io.nack) {
        state := sInitFailed
        initFailedIndex := initIndex
      } .otherwise {
        when (i2cSendReq.ready) {
          i2cSendReq.enq(Cat(initDbHighs(initIndex), "b_000".U, rs))
          initIndex := initIndex + 1.U
          when (initIndex === (initDbHighs.length - 1).U) {
            state := sIdle
          }
        }
      }
    }
    is (sIdle) {
      when (io.writeReq.valid) {
        rs := 1.U
        db := io.writeReq.bits
        state := sSendHigh
      }
    }
    is (sSendHigh) {
      when (i2cSendReq.ready) {
        i2cSendReq.enq(Cat(db(7, 4), "b_000".U, rs))
        state := sSendLow
      }
    }
    is (sSendLow) {
      when (i2cSendReq.ready) {
        i2cSendReq.enq(Cat(db(3, 0), "b_000".U, rs))
        state := sIdle
      }
    }
  }
}

// Handle I2C communication
// For starter, it only supports single byte transmittion with verifing ack.
class I2C extends Module {
  // According to spec, address is 0x27 (010_0111) or 0x3F (011_1111)
  val slaveAddr = 0x27.U
  // According to PCF8574T_datasheet.pdf, max 100kHz,
  // but probably I should take display functioning limit into account too
  // According HD44780 spec, each operation takes about 37 μs?
  val maxSclHz = 10 * 1000
  val i2cPeriod = 2 * Config.hwclkHz / maxSclHz
  // `clkCnt` will count like this (note how it determines io.scl with the fraction of 1/4, 3/4)
  //  _____    __    __    __
  //       |__|  |__|  |__|  |_
  //   <----><----><----><---->
  // Our internal state progress at the moment of "><"

  val io = IO(new Bundle {
    val sendReq = DeqIO(UInt(8.W))
    val nack = Output(Bool())

    // hw
    val scl = Output(Bool())
    val sda = Analog(1.W)
  })

  val sIdle :: sStart :: sTxSlaveAddr :: sVerifyAck1 :: sTxData :: sVerifyAck2 :: sStop :: sNackStop :: Nil = Enum(8)

  val state = RegInit(sIdle)
  val data = Reg(UInt(8.W))
  val clkCnt = Module(new Utils.CounterR(i2cPeriod))
  val ackCnt = Module(new Utils.CounterER(2 * i2cPeriod))
  val txBitIndex = Reg(UInt(3.W))

  val sdaWrapper = Module(new SB_IO_tristate)
  def driveSda(b : UInt) = {
    sdaWrapper.io.float := false.B
    sdaWrapper.io.out := b
  }
  sdaWrapper.io.pin <> io.sda
  sdaWrapper.io.float := true.B
  sdaWrapper.io.out := 0.U

  io.nack := false.B
  io.sendReq.ready := state === sIdle
  io.scl :=
    (state === sIdle) ||
    ((state === sStart) && clkCnt.io.out < (i2cPeriod * 3 / 4).U) ||
    ((i2cPeriod * 1 / 4).U < clkCnt.io.out && clkCnt.io.out < (i2cPeriod * 3 / 4).U) ||
    ((state === sStop) && (i2cPeriod * 1 / 4).U < clkCnt.io.out)
  clkCnt.io.reset := false.B
  ackCnt.io.reset := false.B
  ackCnt.io.en := false.B

  switch (state) {
    is (sIdle) {
      when (io.sendReq.valid) {
        data := io.sendReq.bits
        state := sStart
        clkCnt.io.reset := true.B
      }
    }
    is (sStart) {
      driveSda(0.U)
      when (clkCnt.io.clk) {
        state := sTxSlaveAddr
        txBitIndex := 0.U
      }
    }
    is (sTxSlaveAddr) {
      when (txBitIndex < 7.U) {
        // MSB first
        driveSda(slaveAddr(6.U - txBitIndex))
      } .otherwise {
        // Last bit specifies R/W (0/1)
        driveSda(0.U)
      }
      when (clkCnt.io.clk) {
        txBitIndex := txBitIndex + 1.U
        when (txBitIndex === 7.U) {
          state := sVerifyAck1
          ackCnt.io.reset := true.B
        }
      }
    }
    is (sVerifyAck1) {
      ackCnt.io.en := sdaWrapper.io.in === 0.U
      when (clkCnt.io.clk) {
        when (ackCnt.io.out < (i2cPeriod / 2).U) {
          state := sNackStop
        } .otherwise {
          state := sTxData
          txBitIndex := 0.U
        }
      }
    }
    is (sTxData) {
      driveSda(data(7.U - txBitIndex))
      when (clkCnt.io.clk) {
        txBitIndex := txBitIndex + 1.U
        when (txBitIndex === 7.U) {
          state := sVerifyAck2
          ackCnt.io.reset := true.B
        }
      }
    }
    is (sVerifyAck2) {
      ackCnt.io.en := sdaWrapper.io.in === 0.U
      when (clkCnt.io.clk) {
        when (ackCnt.io.out < (i2cPeriod / 2).U) {
          state := sNackStop
        } .otherwise {
          state := sStop
        }
      }
    }
    is (sStop) {
      driveSda(0.U)
      when (clkCnt.io.clk) {
        state := sIdle
      }
    }
    is (sNackStop) {
      io.nack := true.B
      driveSda(0.U)
      when (clkCnt.io.clk) {
        state := sIdle
      }
    }
  }
}

object LCD {
  val moduleMap : Driver.ModuleMap = Map(
    "LCD" -> Driver.ModuleRegistration(
      () => new LCD,
      () => ???,
      m => ???)
  )
}