package icestorm_study.module

import chisel3._
import chisel3.iotesters.PeekPokeTester
import chisel3.util._
import icestorm_study.{Config, Driver, TopImpl, Utils}

// cf. https://github.com/nesl/ice40_examples
//     https://www.parallax.com/product/27899 (the one I bought)

// For Hex decoding
//  val matrixMap = Seq(
//    Seq( 1, 2,  3, 10),
//    Seq( 4, 5,  6, 11),
//    Seq( 7, 8,  9, 12),
//    Seq(14, 0, 15, 13),
//  )

class MatrixKeyPad extends TopImpl {
  val inner = Module(new MatrixKeyPadInner)
  inner.io.columns := io.keypad_columns
  io.keypad_rows := inner.io.rows
  io.led := inner.io.out(7, 0)
}

// TODO: Multiple key press isn't detected if they share same column (lower row wins)
//       Is it possible to support that case? (probably need to put non-"rowPullIndex" rows into tristate?)
class MatrixKeyPadInner extends Module {
  // Parameters
  val numRows = 4
  val numColumns = 4
  val pollIntervalSec = 0.1
  val pollIntervalCounts = (Config.hwclkHz * pollIntervalSec).toLong
  val rowPullIntervalSec = 0.0001
  val rowPullKntervalCounts = (Config.hwclkHz * rowPullIntervalSec).toLong
  val validKeyPushSec = 0.001 // valid if key pressed 0.001s out of 0.025s (0.1 / 4) for each interval
  val validKeyPushCounts = (Config.hwclkHz * validKeyPushSec).toLong

  val io = IO(new Bundle {
    val out = Output(UInt((numRows * numColumns).W))

    // hw
    val rows = Output(Vec(numRows, Bool()))
    val columns = Input(Vec(numColumns, Bool()))
  })

  // State
  val rowPullIndex = RegInit(0.U(log2Ceil(numRows).W))
  val outState = RegInit(VecInit(Seq.fill(numRows * numColumns){ false.B }))

  val pollInterval = Module(new Utils.CounterR(pollIntervalCounts))
  val rowPullInterval = Module(new Utils.CounterR(rowPullKntervalCounts))
  val keyPushCounters = Seq.fill(numRows * numColumns){ Module(new Utils.CounterER(pollIntervalCounts + 1)) }

  // Connection
  pollInterval.io.reset := false.B
  rowPullInterval.io.reset := false.B
  keyPushCounters.foreach{c =>
    c.io.en := false.B
    c.io.reset := false.B
  }
  io.out := outState.asUInt
  io.rows := VecInit(Seq.fill(4) {true.B})
  io.rows(rowPullIndex) := false.B

  // Rotate which row to pull down
  when (rowPullInterval.io.clk) {
    rowPullIndex := Utils.wrap(rowPullIndex + 1.U, numRows.U)
  }

  // Pusing pad key makes connected column pulled down together.
  // So, we count such events.
  for (r <- 0 until numRows) {
    for (c <- 0 until numColumns) {
      when (rowPullIndex === r.U && !io.columns(c)) {
        keyPushCounters(r * numRows + c).io.en := true.B
      }
    }
  }

  // Update output for each poll interval
  when (pollInterval.io.clk) {
    keyPushCounters.foreach(_.io.reset := true.B)
    for (r <- 0 until numRows) {
      for (c <- 0 until numColumns) {
        outState(r * numRows + c) := keyPushCounters(r * numRows + c).io.out > validKeyPushCounts.U
      }
    }
  }
}

// NOTE: Should use --backend-name verilator for this test
class MatrixKeyPadInnerTester(circuit: MatrixKeyPadInner) extends PeekPokeTester(circuit) {
  val pushInterval = (Config.hwclkHz * 0.3).toLong

  // Simulate pressing key (1, 2)
  val pushedRow = 1
  val pushedCol = 2
  for (_ <- 0 until pushInterval) {
    for (c <- 0 until 4) {
      poke(circuit.io.columns(c), 1) // columns pulled up
    }
    poke(circuit.io.columns(pushedCol), peek(circuit.io.rows(pushedRow)))
    step(1)
  }
  expect(circuit.io.out, 1 << pushedRow * 4 + pushedCol)

  // Simulate pressing multiple keys
  for (_ <- 0 until 10) {
    val row1 = rnd.nextInt(4)
    val col1 = rnd.nextInt(4)
    val row2 = rnd.nextInt(4)
    val col2 = rnd.nextInt(4)
    for (_ <- 0 until pushInterval) {
      for (c <- 0 until 4) {
        poke(circuit.io.columns(c), 1)
      }
      if (col1 == col2) {
        poke(circuit.io.columns(col1), peek(circuit.io.rows(row1)) & peek(circuit.io.rows(row2)))
      } else {
        poke(circuit.io.columns(col1), peek(circuit.io.rows(row1)))
        poke(circuit.io.columns(col2), peek(circuit.io.rows(row2)))
      }
      step(1)
    }
    println(s"Pressed keys: ($row1, $col1), ($row2, $col2)")
    expect(circuit.io.out, (1 << (row1 * 4 + col1)) | (1 << (row2 * 4 + col2)))
  }
}


object MatrixKeyPad {
  val moduleMap : Driver.ModuleMap = Map(
    "MatrixKeyPad" -> Driver.ModuleRegistration(
      () => new MatrixKeyPad,
      () => ???,
      m => ???),
    "MatrixKeyPadInner" -> Driver.ModuleRegistration(
      () => ???,
      () => {
        Config.hwclkHz = 40000
        new MatrixKeyPadInner
      },
      m => new MatrixKeyPadInnerTester(m.asInstanceOf[MatrixKeyPadInner]))
  )
}