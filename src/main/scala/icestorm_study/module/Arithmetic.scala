package icestorm_study.module

import chisel3._
import chisel3.iotesters.PeekPokeTester
import icestorm_study.{Driver}

// Small practice implementing arithmetic operation

class SingleAdder extends Module {
  val io = IO(new Bundle {
    val in0 = Input(UInt(1.W))
    val in1 = Input(UInt(1.W))
    val cin = Input(UInt(1.W))
    val out = Output(UInt(1.W))  // true if 1 or 3 of them are true
    val cout = Output(UInt(1.W)) // true if 2 or 3 of them are true
  })

//  // Mux from truth table
//  val table =
//    """
//      |000 00
//      |001 01
//      |010 01
//      |011 10
//      |100 01
//      |101 10
//      |110 10
//      |111 11
//    """.stripMargin
//  val outs = Utils.truthTableLookup(Cat(io.in0, io.in1, io.cin), table)
//  io.out := outs(0)
//  io.cout := outs(1)

  // Possibly efficient way
  val ins = Seq(io.in0, io.in1, io.cin)

  // parity (ie 1 or 3 or ..)
  io.out := ins.reduce(_ ^ _)

  // i & j = true for some i != j
  var ors = 0.U
  for (i <- 0 until ins.length) {
    for (j <- i + 1 until ins.length) {
      ors = ors | (ins(i) & ins(j))
    }
  }
  io.cout := ors
}

class Adder(val w: Int) extends Module {
  val io = IO(new Bundle {
    val cin = Input(UInt(1.W))
    val in0 = Input(UInt(w.W))
    val in1 = Input(UInt(w.W))
    val out = Output(UInt(w.W))
    val valid = Output(Bool())
  })

  val carry = Wire(Vec(w + 1, UInt(1.W)))
  val tmpOut = Wire(Vec(w, UInt(1.W)))
  carry(0) := io.cin

  for (i <- 0 until w) {
    val m = Module(new SingleAdder)
    m.io.in0     := io.in0(i)
    m.io.in1     := io.in1(i)
    m.io.cin     := carry(i)
    tmpOut(i)    := m.io.out // NOTE: "io.out(i) :=" is not allowed
    carry(i + 1) := m.io.cout
  }
  io.out := tmpOut.asUInt

  // neg + neg
  when (io.in0(w-1) & io.in1(w-1)) {
    io.valid := tmpOut(w-1)

  // neg + non-neg
  } .elsewhen (io.in0(w-1) | io.in1(w-1)) {
    io.valid := true.B

  // non-neg + non-neg
  } .otherwise {
    io.valid := ~tmpOut(w-1)
  }
}

class Subtracter(val w: Int) extends Module {
  val io = IO(new Bundle {
    val in0 = Input(UInt(w.W))
    val in1 = Input(UInt(w.W))
    val out = Output(UInt(w.W))
    val valid = Output(Bool())
  })
  val adder = Module(new Adder(w))
  adder.io.in0 := io.in0
  // Take the minus of 2nd input
  when (io.in1 === 0.U) {
    adder.io.in1 := 0.U
    adder.io.cin := 0.U
  } .otherwise {
    adder.io.in1 := ~io.in1
    adder.io.cin := 1.U
  }
  io.out := adder.io.out

  // If in1 = b_100...00, its negation (011...11 + 1) is overflow as an intermediate value
  // but, subtraction is valid as long as the result is valid
  io.valid := adder.io.valid
}

class AdderTester(c: Adder) extends PeekPokeTester(c) {
  for (_ <- 0 until 10) {
    val in0 = rnd.nextInt(1 << (c.w - 1))
    val in1 = rnd.nextInt(1 << (c.w - 1))
    poke(c.io.cin, 0)
    poke(c.io.in0, in0)
    poke(c.io.in1, in1)
    step(1)
    expect(c.io.out, (in0 + in1) & ((1 << c.w) - 1))
    expect(c.io.valid, (in0 + in1) < (1 << (c.w - 1)))
  }
}

class SubtracterTester(c: Subtracter) extends PeekPokeTester(c) {
  val min = - (1 << (c.w - 1))
  val max = (1 << (c.w - 1)) - 1
  def rndRange(a: Int, b: Int) : Int = {
    rnd.nextInt(b - a + 1) + a
  }
  for (_ <- 0 until 10) {
    val in0 = rndRange(min, max)
    val in1 = rndRange(min, max)
    // it will be naturally sign-extended
    poke(c.io.in0, in0)
    poke(c.io.in1, in1)
    step(1)
    expect(c.io.out, (in0 - in1) & ((1 << c.w) - 1))
    expect(c.io.valid, min <= (in0 - in1) & (in0 - in1) <= max)
  }
}

object Arithmetic {
  val moduleMap : Driver.ModuleMap = Map(
    "Adder" -> Driver.ModuleRegistration(
      () => ???,
      () => new Adder(8),
      m => new AdderTester(m.asInstanceOf[Adder])),
    "Subtracter" -> Driver.ModuleRegistration(
      () => ???,
      () => new Subtracter(8),
      m => new SubtracterTester(m.asInstanceOf[Subtracter])
    )
  )
}