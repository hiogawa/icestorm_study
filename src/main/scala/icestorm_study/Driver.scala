package icestorm_study

import chisel3.{HasChiselExecutionOptions, Module, Driver => ChiselDriver}
import firrtl.{ExecutionOptionsManager, HasFirrtlOptions}
import chisel3.iotesters.{PeekPokeTester, TesterOptionsManager, Driver => ChiselTestDriver}
import icestorm_study.module.{Arithmetic, LCD, MatrixKeyPad, Rot, Uart}

object Driver {
  class MainOptionsManager(name: String)
    extends ExecutionOptionsManager(name)
      with HasChiselExecutionOptions with HasFirrtlOptions

  case class ModuleRegistration
    (genTopImpl: () => TopImpl,
     genTestModule: () => Module,
     genTester: Module => PeekPokeTester[Module])
  // For testing, String key has to be the Module name of dut (test driver seems a bit inflexible)
  type ModuleMap = Map[String, ModuleRegistration]

  val moduleMap : ModuleMap =
    UtilsTest.moduleMap ++
    Rot.moduleMap ++
    Uart.moduleMap ++
    MatrixKeyPad.moduleMap ++
    LCD.moduleMap ++
    Arithmetic.moduleMap

  def findModuleWithWarning[T](args : Seq[String], map: Map[String, T]) : Option[(String, T)] = {
    args match {
      case name :: Nil =>
        map.get(name) match {
          case None =>
            println(s":: Error: Module '$name' not found.")
            None
          case Some(found) => Some(name, found)
        }
      case _ =>
        println(":: Error: One module name needs to be provided.")
        println(":: Available modules are:")
        moduleMap.keys.foreach(println(_))
        None
    }
  }

  val optionsManager = new MainOptionsManager("icestorm_study")
  val testOptionsManager = new TesterOptionsManager

  def main(args: Array[String]): Unit = {
    optionsManager.parse(args)
    findModuleWithWarning(optionsManager.commonOptions.programArgs, moduleMap) match {
      case Some((_, registration)) =>
        ChiselDriver.execute(optionsManager, () => new Top(registration.genTopImpl))
      case None =>
    }
  }

  def testMain(args: Array[String]): Unit = {
    testOptionsManager.parse(args)
    findModuleWithWarning(testOptionsManager.commonOptions.programArgs, moduleMap) match {
      case Some((name, registration)) =>
        testOptionsManager.setTopName(name)
        ChiselTestDriver.execute(registration.genTestModule, testOptionsManager)(registration.genTester)
      case None =>
    }
  }
}

object TestDriver {
  def main(args: Array[String]): Unit = {
    Driver.testMain(args)
  }
}